<!doctype html>
<html lang="ja">
<head>
    <meta charset ="utf-8">
    <style>
        table th{
            background:#eee;
        }
        table th,
        table td {
            border: 1px solid #CCCCCC;
            text-align: center;
            padding: 5px;
        }
        
        form{
            margin-bottom:16px;
        }
        
        p{
            margin-left:80px;
        }
    </style>
</head>
<body>
    <?php
        if(!empty($_POST['calendar'])){
            $year = $_POST['year'];
            $month = $_POST['month'];
        }else{
            $year = date('Y');
            $month =date('m');
        }
        $first_week = date('w',mktime(0,0,0,$month,01,$year));
        $last_day = date('t',mktime(0,0,0,$month+1,0,$year));
        $calendar = array();
        $j =0;
    ?>
    <form method="post" action="" enctype="multipart/form-data">
        <input type="text" name="year">年
        <br>
        <input type="text" name="month">月
        <br>
        <input type="radio" name="check">月曜日始まり
        <input type="radio" name="check">日曜日始まり    
        <input type="submit" name="calendar" value="表示">
    </form>
    <p><?php echo $year; ?>年<?php echo $month; ?>月</p>
    <table>
        <thead>
            <tr>
                <th>日</th>
                <th>月</th>
                <th>火</th>
                <th>水</th>
                <th>木</th>
                <th>金</th>
                <th>土</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                    <?php
                    for ($n =1; $n<$first_week; $n++){
                       echo "<td>"; 
                       echo "</td>";

                    }
                    for ($i =1; $i< $last_day +1;$i++){
                        echo "<td>";
                        $week = date('w',mktime(0,0,0,$month,$i,$year));
                        echo date('d',mktime(0,0,0,$month,$i,$year));
                        echo "</td>";
                        if($week ==0){
                            echo "</tr>";
                            echo "<tr>";
                        }
                        if ($i == 1) {

                            // 1日目の曜日までをループ
                            for ($s = 1; $s <= $week; $s++) {

                                // 前半に空文字をセット
                                $calendar[$j]['day'] = '';
                                $j++;

                            }
                        }

                    }
                    ?>
            </tr>
        </tbody>
    </table>
</body>
